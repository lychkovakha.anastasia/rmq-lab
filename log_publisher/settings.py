import logging.config

DEAD_EXCHANGE_NAME = 'dlx'
DEAD_QUEUE_NAME = 'dlx_queue'
DEAD_ROUTING_KEY = ["dead.#"]

EXCHANGE_NAME = 'topic_logs'
QUEUE_NAME = 'queue_logs'
ROUTING_KEY = ['#.error', 'info.#']

dict_config = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'detailed': {
            'class': 'logging.Formatter',
            'format': '%(asctime)s %(levelname)s %(name)s: %(message)s'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'INFO',
            'formatter': 'detailed',
        },
    },
    'root': {
        'level': 'INFO',
        'handlers': ['console']
    },
}

logging.config.dictConfig(dict_config)