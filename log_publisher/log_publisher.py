import pika
from MQClient import MQClient
from settings import *
from uuid import uuid4

def msg_publisher(channel, *, exchange, routing_key):
    data = str(uuid4())
    channel.basic_publish(
            exchange=exchange,
            routing_key=routing_key,
            properties=pika.BasicProperties(
                content_type='text/plain',
                delivery_mode=1), 
            body=data,
            mandatory=True)
    print('[Producer] Message was published')

if __name__ == "__main__":
    client = MQClient(
        username="alych",
        password="alych",
    )
    channel = client.get_channel()
    client.connect()
    msg_publisher(channel,
                  exchange=EXCHANGE_NAME,
                  routing_key="kernel.error")
    msg_publisher(channel,
                  exchange=EXCHANGE_NAME,
                  routing_key="info.kernel.status")
    msg_publisher(channel,
                  exchange=EXCHANGE_NAME,
                  routing_key="warning.kernel")
    msg_publisher(channel,
                  exchange=EXCHANGE_NAME,
                  routing_key="kernel.dead.error")