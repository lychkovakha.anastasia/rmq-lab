import time
import logging
from MQClient import MQClient
from settings import *

logger = logging.getLogger(__name__)

def msg_consumer(channel, method_frame, header_frame, body):
    logger.info(f'Processing message. Routing_key:{method_frame.routing_key} body:{body}')
    try:
        logger.info(f'Message received by consumer')
        if "dead" in method_frame.routing_key:
            raise Exception("Simulated exception in consumer")
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
    except Exception as e:
        logger.info(f"Error: {e}. Sending it to dead letter queue")
        channel.basic_nack(delivery_tag=method_frame.delivery_tag,requeue=False)
    return

def dlq_consumer(channel, method_frame, header_frame, body):
    logger.info('Dead message `%s`', body)
    channel.basic_ack(delivery_tag=method_frame.delivery_tag)

if __name__ == "__main__": 
    client = MQClient(
        username="alych",
        password="alych",
    )
    channel = client.get_channel()
    
    client.connect()
    logger.info('Consuming.')

    channel.basic_consume(
        queue=QUEUE_NAME,
        on_message_callback=msg_consumer,
        auto_ack=False,
        consumer_tag='log-consumer'
    )
    channel.basic_consume(
        queue=DEAD_QUEUE_NAME,
        on_message_callback=dlq_consumer,
        auto_ack=False,
        consumer_tag='dlq-consumer'
    )

    channel.start_consuming()
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()
    client.connection.close()