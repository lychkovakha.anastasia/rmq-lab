import pika
from settings import *

class MQClient(object):
    def __init__(self, *, mq_host = 'rabbitmq', username, password) -> None:
        credentials = pika.PlainCredentials(username, password)
        conn_params = pika.ConnectionParameters(
            host=mq_host,
            credentials=credentials
        )
        self.connection = pika.BlockingConnection(conn_params)
        self.channel = self.connection.channel()

    def get_channel(self):
        return self.channel

    def create_exchange(self, exchange_name):
        self.channel.exchange_declare(
            exchange=exchange_name,
            exchange_type='topic',
            passive=False,
            durable=True,
            auto_delete=False
        )
                
    def queue_bind(self, exchange_name, queue_name, routing_key):
        self.channel.queue_bind(
            queue=queue_name,
            exchange=exchange_name,
            routing_key=routing_key,
        )
    
    def create_queue(self, exchange_name, queue_name, routing_keys, is_dead=False):
        arguments = {}
        if not is_dead:
            arguments = {
                "x-message-ttl": 1000,
                "x-dead-letter-exchange": DEAD_EXCHANGE_NAME,
                "x-dead-letter-routing-key": DEAD_ROUTING_KEY[0],
            }
        self.channel.queue_declare(
            queue=queue_name,
            arguments=arguments
        )
        for routing_key in routing_keys:
            self.queue_bind(exchange_name, queue_name, routing_key)

    def connect(self):
        self.create_exchange(EXCHANGE_NAME)
        self.create_queue(EXCHANGE_NAME, QUEUE_NAME, ROUTING_KEY)
        self.create_exchange(DEAD_EXCHANGE_NAME)
        self.create_queue(DEAD_EXCHANGE_NAME,DEAD_QUEUE_NAME, DEAD_ROUTING_KEY, True)